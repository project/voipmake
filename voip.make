; makefile for basic voip-ready Drupal

name = "VoIP Drupal"

core = 6.x
api = 2

; checkout Drupal 6.x core:
projects[drupal][type] = core

; Use pressflow instead of Drupal core:
;projects[pressflow][type] = "core"
;projects[pressflow][download][url] = "http://launchpad.net/pressflow/6.x/6.22.104/+download/pressflow-6.22.104.tar.gz"
;projects[pressflow][download][type] = "get"

; Contrib
; --------
; Required contributed modules outside the VoIP Drupal project. 

projects[] = audiofield
projects[] = cck
projects[] = content_profile
projects[] = ctools
projects[] = date
projects[] = features
projects[] = filefield
projects[] = filefield_sources
projects[] = getid3
projects[] = google_analytics
projects[] = messaging
projects[] = mimemail
projects[] = strongarm
projects[] = swftools
projects[] = views

; Optional modules required for testing VoIP Drupal extended capabilities 
projecs[] = phone
projects[] = smsframework
projects[] = soundcloud_tools

; Helper modules
projects[] = admin_menu
projects[] = advanced_help
projects[] = devel
;;projects[] = diff

; VoIP Drupal
; -----------

; VoIP Drupal core
projects[] = voipdrupal

; Non-core VoIP Drupal modules
projects[] = audioconverter
projects[] = audiorecorderfield
projects[] = click2call
projects[] = messaging_voip
projects[] = phonerecorderfield
projects[] = voipextension
projects[] = voipnode
projects[] = voipnumber
projects[] = voipnumberfield
projects[] = voipphone
projects[] = voipvoice

; Experimental modules
;projects[voipusernumber][version] = "6.x"
;projects[voipusernumber][type] = "module"

